package com.rentamaid.security.restfulwebservices.jwt;

public record JwtTokenRequest(String username, String password) {}


