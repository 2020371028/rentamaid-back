
package com.rentamaid.security.restfulwebservices.entity;

/**
 *
 * @author oscarcortes
 */
public enum EstadoVacante {
    PENDIENTE,
    FINALIZADO
}
