/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package com.rentamaid.security.restfulwebservices.entity;

/**
 *
 * @author Jose
 */
public enum Estado {
    PENDIENTE,
    RECHAZADO,
    ACEPTADO
}
